#pragma once
#include "Entity.h"

static const sf::Color PACKMAN_COLOR = sf::Color(255, 216, 0);
static const float PACKMAN_SPEED = 120.f; // pixels per second.
static const float PACKMAN_RADIUS = 16.f; // pixels

class Pacman : public MovingEntity
{
public:
    Pacman(int x, int y) {
        direction = Direction::NONE;
        shape.setRadius(PACKMAN_RADIUS);
        shape.setFillColor(PACKMAN_COLOR);
        shape.setPosition(sf::Vector2f(x, y));
        xPos = x;
        yPos = y;
       
    }
    sf::Vector2f Getpos() {
        return shape.getPosition();
    }
        void updatePacmanDirection() {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
            {
                direction = Direction::UP;
            }
            else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            {
                direction = Direction::DOWN;
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
            {
                direction = Direction::LEFT;
            }
            else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            {
                direction = Direction::RIGHT;
            }
        }

        void update(float elapsedTime) {
            const float step = PACKMAN_SPEED * elapsedTime;
            sf::Vector2f movement(0.f, 0.f);
            switch (direction)
            {
            case Direction::UP:
                movement.y -= step;
                break;
            case Direction::DOWN:
                movement.y += step;
                break;
            case Direction::LEFT:
                movement.x -= step;
                break;
            case Direction::RIGHT:
                movement.x += step;
                break;
            case Direction::NONE:
                break;
            }
            shape.setFillColor(sf::Color(255, 216, 0));
            shape.move(movement);
        }

        void updateSup(float elapsedTime) {
            const float step = PACKMAN_SPEED * elapsedTime*1.2f;
            sf::Vector2f movement(0.f, 0.f);
            switch (direction)
            {
            case Direction::UP:
                movement.y -= step;
                break;
            case Direction::DOWN:
                movement.y += step;
                break;
            case Direction::LEFT:
                movement.x -= step;
                break;
            case Direction::RIGHT:
                movement.x += step;
                break;
            case Direction::NONE:
                break;
            }
            auto measure = shape.getPosition();
            if (int(measure.x + measure.y) % 6 >= 3) {
                shape.setFillColor(sf::Color(255, 0, 0));
            }
            else {
                shape.setFillColor(sf::Color(255, 216, 0));
            }
            shape.move(movement);
        }


};
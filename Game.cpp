#include <iostream>
#include "Game.h"
static const char FIELD_MAZE[] =
"\
####################\n\
#*              #  #\n\
# ###### ###### ## #\n\
#               ## #\n\
# ###### #### # ## #\n\
#             #    #\n\
# ##### ##### # ####\n\
#                  #\n\
##### ## ## ###### #\n\
#   # ##  #      # #\n\
#    *### # ####*# #\n\
#   # ##  #      # #\n\
##### ## ## ###### #\n\
#     ## ##        #\n\
# ### #  ## ### #  #\n\
# ### # ##*     # ##\n\
#   #      # ##    #\n\
## #### #### ####  #\n\
##*               *#\n\
####################";
int sizec = 50;
Game::Game() {
    End = false;
    pacman = new Pacman(860, 60);
    int tempx = 0;
    int tempy = 0;
    fact.push_back( new BlinkyFactory);
    fact.push_back(new InkyFactory);
    fact.push_back(new PinkyFactory);
    fact.push_back(new ClydeFactory);
    for (auto fa : fact) {
        ghosts.push_back(fa->createGhost());
    }
    for (auto let : FIELD_MAZE) {
        if (let == '#') {
            cells.push_back(new Cell(tempx, tempy, sizec));
            tempx += sizec;
        }
        else if (let == ' ') {
            objects.push_back(new PacGum(tempx + 21, tempy + 21, 8));
            tempx += sizec;
        }
        else if (let == '*') { 
            objects.push_back(new SuperPacGum(tempx + 15, tempy + 15, 20));
            tempx += sizec;
        }
        else if (let == '\n') { tempx = 0; tempy += sizec; }

    }

    
    
    //create ghosts and add them into objects and ghosts
    //if . - create PacGum into objects
    //if o - create SuperPacGum
    //if - - create GhostHouse 
}
void Game::PacCellColl() {
    pacman->updatePacmanDirection();
    float distt = 42;
    switch (pacman->Getdir())
    {
    case Direction::UP:
        for (auto celle : cells) {
            float cx = celle->Getpos().x;
            float cy = celle->Getpos().y;
            float px = pacman->Getpos().x;
            float py = pacman->Getpos().y;
            if (py>cy && std::abs(py-cy-9.f)< (distt) && std::abs(px-cx-9.f)< (distt - 1)) {
                Direction d = Direction::NONE;
                pacman->Setdir(d);
                break;
            }
        }
        break;
    case Direction::DOWN:
        for (auto celle : cells) {
            float cx = celle->Getpos().x;
            float cy = celle->Getpos().y;
            float px = pacman->Getpos().x;
            float py = pacman->Getpos().y;
            if (py < cy && std::abs(py - cy - 9.f) < (distt) && std::abs(px - cx - 9.f) < (distt - 1)) {
                Direction d = Direction::NONE;
                pacman->Setdir(d);
                break;
            }
        }
        break;
    case Direction::LEFT:
        for (auto celle : cells) {
            float cx = celle->Getpos().x;
            float cy = celle->Getpos().y;
            float px = pacman->Getpos().x;
            float py = pacman->Getpos().y;
            if (px > cx && std::abs(px - cx - 9.f) < (distt) && std::abs(py - cy - 9.f) < (distt - 1)) {
                Direction d = Direction::NONE;
                pacman->Setdir(d);
                break;
            }
        }
        break;
    case Direction::RIGHT:
        for (auto celle : cells) {
            float cx = celle->Getpos().x;
            float cy = celle->Getpos().y;
            float px = pacman->Getpos().x;
            float py = pacman->Getpos().y;
            if (px < cx && std::abs(px - cx - 9.f) < (distt) && std::abs(py - cy - 9.f) < (distt-1)) {
                Direction d = Direction::NONE;
                pacman->Setdir(d);
                break;
            }
        }
        break;
    case Direction::NONE:
        break;
    }
}
int Game::eating() {
    int temp = 0;
    auto iter = objects.begin();
    for (auto ob : objects) {
        float gx = ob->Getpos().x;
        float gy = ob->Getpos().y;
        float px = pacman->Getpos().x;
        float py = pacman->Getpos().y;
        if (std::abs(px - gx + 16.f - ob->GetSize()/2) < (16) && std::abs(py - gy + 16.f - ob->GetSize()/2) < (16)) {
            int points = ob->Getw();
            objects.erase(iter + temp);
            temp += 1;
            if (points == 100) {
                Sup.restart();
                Super = true;
            }
            return points;
        }
        temp += 1;
    }
    return 0;

}
int Game::eatingG() {
    for (auto g : ghosts) {
        float gx = g->Getpos().x;
        float gy = g->Getpos().y;
        float px = pacman->Getpos().x;
        float py = pacman->Getpos().y;
        if (g->edible() && std::abs(px - gx) < (32) && std::abs(py - gy) < (32)) {
            g->eaten();
            return 500;
            }
            
        }
    return 0;
}
void Game::Geating() {
    for (auto g : ghosts) {
        float gx = g->Getpos().x;
        float gy = g->Getpos().y;
        float px = pacman->Getpos().x;
        float py = pacman->Getpos().y;
        if (g->edible() && std::abs(px - gx) < (32) && std::abs(py - gy) < (32)) {
            End = true;
            
        }
    }
}
bool Game::SuperIf(){
    if (Super && Sup.getElapsedTime().asSeconds() < 5) {
        return true;
    }
    Super = false;
    return false;
}

void Game::updateGame( float elapsedTime) {
    points += eating();
    PacCellColl();
    if (SuperIf()) {
        pacman->updateSup(elapsedTime);
        points += eatingG();
    }
    else {
        pacman->update(elapsedTime);
        Geating();
    }
    

}

void Game::render(sf::RenderWindow& window) const {
    if (End) {
        sf::Texture go;
        go.loadFromFile("C:\\Users\\tadss\\Downloads\\go.png");
        while (window.isOpen()) {
            sf::RectangleShape pase(sf::Vector2f(1000, 1000));
            pase.setTexture(&go);
            window.clear(sf::Color::Black);
            window.draw(pase);
            window.display();
            sf::Event event;
            while (window.pollEvent(event))
            {
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                {
                    window.close();
                }
                else if (event.type == sf::Event::Closed)
                {
                    window.close();
                }
            }
        }
    }
    for (auto c : cells) {
        c->render(window);
    }
    for (auto obj : objects) {
        obj->render(window);
    }
    for (auto g : ghosts) {
        g->render(window);
        pacman->render(window);
    }
}

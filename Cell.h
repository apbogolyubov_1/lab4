#pragma once
#include "Entity.h"
#include<iostream>

class Cell: public StaticEntity
{
public:
		
	Cell(int x, int y, int s) {
		xPos = x;
		yPos = y;
		size = s;
		shape.setFillColor(sf::Color(52, 93, 199));
		shape.setSize(sf::Vector2f(size, size));
		shape.setPosition(sf::Vector2f(xPos, yPos));
	}

	void render(sf::RenderWindow& window) {
		
		window.draw(shape);
	}

};

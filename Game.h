#pragma once
#include <vector>
#include <SFML/Graphics.hpp>
#include "Pacman.h"
#include "Entity.h"
#include "Ghost.h"
#include "Cell.h"


class ScoreBoard
{
    std::vector<sf::Texture> tex;
    sf::Texture sc;

public:
    ScoreBoard() {
        sf::Texture c0;
        sf::Texture c1;
        sf::Texture c2;
        sf::Texture c3;
        sf::Texture c4;
        sf::Texture c5;
        sf::Texture c6;
        sf::Texture c7;
        sf::Texture c8;
        sf::Texture c9;
        sc.loadFromFile("C:\\Users\\tadss\\Downloads\\score.png");
        c0.loadFromFile("C:\\Users\\tadss\\Downloads\\c0.png");
        tex.push_back(c0);
        c1.loadFromFile("C:\\Users\\tadss\\Downloads\\c1.png");
        tex.push_back(c1);
        c2.loadFromFile("C:\\Users\\tadss\\Downloads\\c2.png");
        tex.push_back(c2);
        c3.loadFromFile("C:\\Users\\tadss\\Downloads\\c3.png");
        tex.push_back(c3);
        c4.loadFromFile("C:\\Users\\tadss\\Downloads\\c4.png");
        tex.push_back(c4);
        c5.loadFromFile("C:\\Users\\tadss\\Downloads\\c5.png");
        tex.push_back(c5);
        c6.loadFromFile("C:\\Users\\tadss\\Downloads\\c6.png");
        tex.push_back(c6);
        c7.loadFromFile("C:\\Users\\tadss\\Downloads\\c7.png");
        tex.push_back(c7);
        c8.loadFromFile("C:\\Users\\tadss\\Downloads\\c8.png");
        tex.push_back(c8);
        c9.loadFromFile("C:\\Users\\tadss\\Downloads\\c9.png");
        tex.push_back(c9);
    }
    ~ScoreBoard() = default;
    sf::Texture gett(int num) {
        return tex[num];
    }
    void render(sf::RenderWindow& window,int points) {
        sf::RectangleShape pase(sf::Vector2f(125, 50));
        std::string nums=std::to_string(points);
        pase.setTexture(&sc);
        int posx = 200;
        while(points>0) {
            sf::RectangleShape temp(sf::Vector2f(25, 50));
            sf::Texture temp1 = gett(points%10);
            temp.setTexture(&temp1);
            temp.setPosition(posx, 0);
            window.draw(temp);
            points = points / 10;
            posx -= 25;
        }
        window.draw(pase);
    }
};

class Game
{
    bool End;
	int width;
	int height;
	int points;
	std::vector<StaticEntity*> objects;
	std::vector<Ghost*> ghosts;
	std::vector<Cell*> cells;
	Pacman* pacman;
    bool Super;
    sf::Clock Sup;

public:
	Game();
    std::vector<GhostFactory*> fact;
	std::vector<StaticEntity*> getEntities();
	std::vector<Cell> getCells();
	Pacman getPacman() ;
	void updateGame(float elapsedTime); // изменения координат объектов
	void render(sf::RenderWindow& window) const; // отрисовка
	~Game() = default;
	void PacCellColl();
	int eating();
    int eatingG();
    void Geating();
    bool SuperIf();
	const int GetPoints() { return points; }
};

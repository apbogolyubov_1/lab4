#pragma once
#include <SFML/Graphics.hpp>
enum struct Direction
{
	NONE,
	UP,
	DOWN,
	LEFT,
	RIGHT
};


class Entity
{
protected:
	int xPos;
	int yPos;
	int size;
	

public:
	virtual int Getw() { return 0; };
	virtual sf::Vector2f Getpos() {
		return sf::Vector2f(xPos, yPos);
	}
	virtual sf::FloatRect getBounds() = 0;
	//virtual void update() = 0;
	virtual void render(sf::RenderWindow& window) = 0;
	void death() {
		this->~Entity();
	}
};

class StaticEntity : public Entity {
protected:
	sf::FloatRect bounds;
	sf::RectangleShape shape;
public:
	void SetColor(sf::Color c) { shape.setFillColor(c); }
	int GetSize() { return size; }
	sf::FloatRect getBounds() override {
		return shape.getGlobalBounds();
	}
};

class PacGum : public StaticEntity
{
	int weight = 10;
public:
	PacGum(int x, int y, int s) {
		xPos = x;
		yPos = y;
		size = s;
		shape.setFillColor(sf::Color(255, 183, 174));
		shape.setSize(sf::Vector2f(size, size));
		shape.setPosition(sf::Vector2f(xPos, yPos));

	}
	sf::Vector2f Getpos() {
		return shape.getPosition();
	}
	int Getw() { return weight; }
	void render(sf::RenderWindow& window) {
		window.draw(shape);
	}
};




class SuperPacGum : public StaticEntity
{
	int weight = 100;
public:

	SuperPacGum(int x, int y, int s) {
		xPos = x;
		yPos = y;
		size = s;
		shape.setFillColor(sf::Color(255, 183, 174));
		shape.setSize(sf::Vector2f(size, size));
		shape.setPosition(sf::Vector2f(xPos, yPos));
	}
	sf::Vector2f Getpos() {
		return shape.getPosition();
	}
	int Getw() { return weight; }
	void render(sf::RenderWindow& window) {
		window.draw(shape);
	}
};


class MovingEntity : public Entity {
protected:
	int speed;
	sf::Vector2i speedVec;
	Direction direction;
	sf::CircleShape shape;

	//etc.

public:
	Direction Getdir() { return direction; }
	void Setdir(Direction &d) { direction=d; }
	void unpdatePosition() {

	}
	virtual void render(sf::RenderWindow& window) {
		window.draw(shape);
	}
	sf::FloatRect getBounds() {
		return shape.getGlobalBounds();
	}
	//setters, getters, etc. 
};
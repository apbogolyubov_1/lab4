#include <SFML/Graphics.hpp>
#include "game.h"



void handleEvents(sf::RenderWindow& window, bool *flag)
{
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            window.close();
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
        {
            if (event.type == sf::Event::KeyReleased) {
                *flag = !*flag;
            }
        }
        // Кнопка закрытия окна
        else if (event.type == sf::Event::Closed)
        {
            window.close();
        }
    }
}
bool startKey(sf::RenderWindow& window) {
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            window.close();
        }
        // Кнопка закрытия окна
        else if (event.type == sf::Event::Closed)
        {
            window.close();
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
        {
            return false;
        }
        return true;
    }
    return true;
}

void start(sf::Clock& clock, sf::RenderWindow& window, sf::Texture st) {
    sf::RectangleShape pase(sf::Vector2f(1000, 1000));
    pase.setTexture(&st);
    window.clear(sf::Color::Black);
    window.draw(pase);
    window.display();
    clock.restart();
}

void update(sf::Clock& clock, Game& game)
{
    const float elapsedTime = clock.getElapsedTime().asSeconds();
    clock.restart();
    game.updateGame(elapsedTime);
}

void render(sf::RenderWindow& window, Game& game, ScoreBoard s)
{
    window.clear();
    game.render(window);
    s.render(window, game.GetPoints());
    window.display();
}
void Paused(sf::Clock& clock, sf::RenderWindow& window, sf::Texture pau) {
    sf::RectangleShape pase(sf::Vector2f(200, 200));
    pase.setTexture(&pau);
    pase.setPosition(400, 400);
    window.clear(sf::Color::Black);
    window.draw(pase);
    window.display();
    clock.restart();
}

int main()
{
    sf::Texture pau;
    pau.loadFromFile("C:\\Users\\tadss\\Downloads\\pau.png");
    sf::Texture st;
    st.loadFromFile("C:\\Users\\tadss\\Downloads\\press-start.png");
   
    bool flag = false;
    sf::RenderWindow window(sf::VideoMode(1000, 1000), "PacMan Game Clone", sf::Style::Close);
    sf::Clock clock;
    start(clock, window, st);
    while(startKey(window)){}
    Game game;
    ScoreBoard s;
    while (window.isOpen())
    {

        handleEvents(window, &flag);
        if (flag) {
            Paused(clock, window, pau);
        }
        else {
            update(clock, game);
            render(window, game, s);
        }
    }

    return 0;
}
#pragma once
#include "Entity.h"
#include "Cell.h"
#include "Pacman.h"
static const float GHOST_SPEED = 10.f; // pixels per second.
static const float GHOST_RADIUS = 16.f; // pixels
enum struct Mode
{
    Eaten,
    House,
    Frightened,
    Chase,
    Scatter
};

static const sf::Color GHOST_COLOR1 = sf::Color(255, 0, 0);
static const sf::Color GHOST_COLOR2 = sf::Color(255, 0, 255);
static const sf::Color GHOST_COLOR3 = sf::Color(255, 255, 0);
static const sf::Color GHOST_COLOR4 = sf::Color(0, 0, 255);


class Ghost : public MovingEntity
{protected:
    int w = 500;
    Mode mode;
    sf::Clock InnerTimer;
public:
    Direction currentDirection;
    Ghost() {
        shape.setRadius(GHOST_RADIUS);
        shape.setFillColor(PACKMAN_COLOR);
        shape.setPosition(sf::Vector2f(100, 500));
        xPos = 100;
        yPos = 500;
        
    }
    void updatePos(sf::Vector2f m){
        shape.move(m);
     }
    void eaten() {
        mode = Mode::Eaten;
        InnerTimer.restart();
        shape.setFillColor(sf::Color::White);
    }
    bool edible() {
        return(mode!=Mode::Eaten);
    }
    int getw() { return w; }
    sf::Vector2f Getpos() {
        return shape.getPosition();
    }
};


class Blinky : public Ghost {
public:
    Blinky(){
        w = 500;
        mode = Mode::Chase;
        shape.setRadius(GHOST_RADIUS);
        shape.setFillColor(GHOST_COLOR1);
        shape.setPosition(sf::Vector2f(100, 500));
        xPos = 100;
        yPos = 500;
    }
};
class Clyde : public Ghost {
public:
    Clyde() {
        w = 500;
        mode = Mode::Chase;
        shape.setRadius(GHOST_RADIUS);
        shape.setFillColor(GHOST_COLOR2);
        shape.setPosition(sf::Vector2f(850, 900));
        xPos = 100;
        yPos = 500;
    }
    
 
};
class Inky : public Ghost {
public:
    Inky() {
        w = 500;
        mode = Mode::Chase;
        shape.setRadius(GHOST_RADIUS);
        shape.setFillColor(GHOST_COLOR3);
        shape.setPosition(sf::Vector2f(200, 900));
        xPos = 100;
        yPos = 500;
    }
};
class Pinky : public Ghost {
public:
    Pinky() {
        w = 500;
        mode = Mode::Chase;
        shape.setRadius(GHOST_RADIUS);
        shape.setFillColor(GHOST_COLOR4);
        shape.setPosition(sf::Vector2f(100, 500));
        xPos = 100;
        yPos = 500;
    }
};
class GhostFactory {
public:
   virtual Ghost* createGhost() = 0;
    ~GhostFactory() {}
};

class BlinkyFactory : public GhostFactory {
    Ghost* createGhost() {
        return new Blinky;
    }
};

class ClydeFactory : public GhostFactory {
    Ghost* createGhost(){
        return new Clyde;
    }
};

class InkyFactory : public GhostFactory {
    Ghost* createGhost(){
        return new Inky;
    }
};

class PinkyFactory : public GhostFactory {
    Ghost* createGhost(){
        return new Pinky;
    }
};
